defmodule Spartanball.Repo.Migrations.CreateOwner do
  use Ecto.Migration

  def change do
    create table(:owners) do
      add :name, :string
      add :email, :string
      add :team_name, :string

      timestamps()
    end

  end
end
