defmodule Spartanball.Repo.Migrations.CreatePerformance do
  use Ecto.Migration

  def change do
    create table(:performances) do
      add :points, :integer, default: 0
      add :wins, :integer, default: 0
      add :losses, :integer, default: 0
      add :ties, :integer, default: 0
      add :goals_scored, :integer, default: 0
      add :goals_conceded, :integer, default: 0
      add :owner_id, references(:owners, on_delete: :nothing)

      timestamps()
    end
    create index(:performances, [:owner_id])

  end
end
