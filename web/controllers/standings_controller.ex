defmodule Spartanball.StandingsController do
  use Spartanball.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
