defmodule Spartanball.OwnerController do
  use Spartanball.Web, :controller

  alias Spartanball.Owner
  alias Spartanball.Performance

  def index(conn, _params) do
    owners = Repo.all(Owner)
    render(conn, "index.html", owners: owners)
  end

  def new(conn, _params) do
    changeset = Owner.changeset(%Owner{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"owner" => owner_params}) do
    changeset = Owner.changeset(%Owner{}, owner_params)

    case Repo.insert(changeset) do
      {:ok, owner} ->
        Repo.insert(%Performance{owner_id: owner.id})
        conn
        |> put_flash(:info, gettext("Owner created successfully."))
        |> redirect(to: owner_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    owner = Repo.get!(Owner, id)
    render(conn, "show.html", owner: owner)
  end

  def edit(conn, %{"id" => id}) do
    owner = Repo.get!(Owner, id)
    changeset = Owner.changeset(owner)
    render(conn, "edit.html", owner: owner, changeset: changeset)
  end

  def update(conn, %{"id" => id, "owner" => owner_params}) do
    owner = Repo.get!(Owner, id)
    changeset = Owner.changeset(owner, owner_params)

    case Repo.update(changeset) do
      {:ok, owner} ->
        conn
        |> put_flash(:info, gettext("Owner updated successfully."))
        |> redirect(to: owner_path(conn, :show, owner))
      {:error, changeset} ->
        render(conn, "edit.html", owner: owner, changeset: changeset)
    end
  end

end
