defmodule Spartanball.Owner do
  use Spartanball.Web, :model

  schema "owners" do
    field :name, :string
    field :email, :string
    field :team_name, :string

    has_one :performance, Spartanball.Performance, on_delete: :delete_all
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :email, :team_name])
    |> validate_required([:name, :email, :team_name])
  end
end
