defmodule Spartanball.Performance do
  use Spartanball.Web, :model

  schema "performances" do
    field :points, :integer, default: 0
    field :wins, :integer, default: 0
    field :losses, :integer, default: 0
    field :ties, :integer, default: 0
    field :goals_scored, :integer, default: 0
    field :goals_conceded, :integer, default: 0
    belongs_to :owner, Spartanball.Owner

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:owner_id])
    |> validate_required([:owner_id])
  end
end
