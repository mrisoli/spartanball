# Spartanball

Spartanball is a simple app to manage a friendly league, including teams, matches, and standings tracker. I made this to test my knowledge in Elixir and Phoenix.

## Goals

  * Provide quality code and understandable documentation that is up to date
  * Make a simple, yet useful app
  * Make it easily testable and easily expandable
  * Improve it as much as possible
  * Use as a sandbox for any other useful tools

## Features

  * ES2015
  * Phoenix Slime

## Requirements

  * Elixir 1.3 or newer
  * Phoenix 1.2
  * NodeJS 7 or newer
  * Brunch 2.7 or newer

To start spartanball:

  * Clone this repository
  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `npm install`
  * Start Phoenix endpoint with `mix phoenix.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix

