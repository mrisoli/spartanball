defmodule Spartanball.Factory do
  # with Ecto
  use ExMachina.Ecto, repo: Spartanball.Repo

  def owner_factory do
    %Spartanball.Owner{
      name: "Marcelo Risoli",
      email: sequence(:email, &"email-#{&1}@example.com"),
      team_name: "Balacobaco"
    }
  end
end
