defmodule Spartanball.PerformanceTest do
  use Spartanball.ModelCase

  alias Spartanball.Performance

  @valid_attrs %{owner_id: 1}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Performance.changeset(%Performance{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Performance.changeset(%Performance{}, @invalid_attrs)
    refute changeset.valid?
  end
end
