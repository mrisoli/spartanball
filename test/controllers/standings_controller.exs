defmodule Spartanball.StandingsControllerTest do
  use Spartanball.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "This is Spartanball!"
  end
end
